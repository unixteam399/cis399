Purpose:
This document will specify the steps in order to replicate our initial install of Ubuntu. 
We downloaded the 14.04/64bit version of ubuntu, because it's the newest one, and the memory could be larger than 4Gb.
Procedure:
Before the installation, we need UltraISO to write the ubuntu ISO file(which downloaded from www.ubuntu.com) into flash drive.
Make sure the flash drive is NTFS/FAT32 format, then use UltraISO to load image file.
Our aim is to boot from flash drive, so click on the "Syslinux" option under "write into boot sector", otherwise PC can not be
booted.
Then click write. After several minutes, our boot flash drive is ready.
1.	Boot to the media device. (We used USB storage).
2.	Click on Install Ubuntu.(instead of experience ubuntu)
3.	Do not download update or install third party software and click continue.
4.	There's a Windows XP OS already installed in computer, 
	the 2 options are equally assign the space for windows and linux,
	or erase disk for brand new installation.
	We choose to erase disk (if data is backed up) and install now.
	Because we concerned dual system would cause unstable.
5.	Select Region and language (we used English).
6.	Fill in user information
	a.	Name: Admin
	b.	Computers name: Team
	c.	Password: Password1
	d.	Select Require password
7.	 Restart computer as needed.
8.	Login to the account and make sure the installation is working.
